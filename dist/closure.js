
var clickCount = "Opps I am a outside junk value sfk !";
var that = this;

var incrementClickCount = (function () {
  var clickCount = 0;
  return function () {
    console.log("this.clickCount => ", this.clickCount);
    return ++clickCount;
  }
})();

// Checking the scope here
// console.log("Accessing the 'clickCount' outside to check the scope => ", clickCount);
