var message = 'Hello sfk !!';

function helloWorld() {
  greeting = "Hello";
  console.log("Accessing the variable 'greeting' inside func => ", greeting);
}

// helloWorld();
// console.log("Accessing the variable 'greeting' outside func => ", greeting);
// messedUp();

// when we declare message inside the function like in our Example
// log prints 'undefined'. Thats the side effect of varaiable hosting.
// Braces wont create scope in javascript.
function messedUp() {
  console.log(message);
  var message = "Oops got messed up";
}
