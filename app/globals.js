
export let content = $('#content');

export function preventRightClick(flag) {

  if (flag === false) {
    return;
  }

  $("body").contextmenu(function(event) {
    event = event || window.event;
    if (event.preventDefault) {
      event.preventDefault();
    } else {
      event.returnValue = false;
    }
  });
}
