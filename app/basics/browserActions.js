export function run() {
  g.preventRightClick(true);

  g.content.append(`
    <h3> Browser events demo </h3>
    <input type="button" value="Click Me" id="btn"/>
    <input type="button" value="Clear Me"/>
    <br/><br/>
    <div id="textArea"/>`
  );

  $("#btn").mouseup(whichMouseButtonClicked);
}

function whichMouseButtonClicked(e) {
  var whichButton;

  if (e.which) {
    switch (event.which) {
      case 1:
        whichButton = "left button clicked";
        break;
      case 2:
        whichButton = "middle button clicked";
        break;
      case 3:
        whichButton = "right button clicked";
        break;
      default:
        whichButton = "Invalid button clicked";
        break;
    }
  } else {
    switch (event.button) {
      case 1:
        whichButton = "left button clicked";
        break;
      case 4:
        whichButton = "middle button clicked";
        break;
      case 2:
        whichButton = "right button clicked";
        break;
      default:
        whichButton = "Invalid button clicked";
        break;
    }
  }

  $("#textArea").append(`<br/> Result is => ${whichButton}`);
}
