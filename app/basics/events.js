var content = document.getElementById('content');

export function run() {
  content.innerHTML = `
    <h3> Events demo </h3>
    <input type="button" id="btnTest" value="Test"/>
  `;

  $(document).ready(function(){
    $("#btnTest").mouseover(changeColorOnMouseOver);
    $("#btnTest").mouseout(changeColorOnMouseOut);
    $("#btnTest").click(onClick);
  });
}

function onClick(event) {
  var eventDetails = `
    <br/>
    <br/>
    Event Name = ${event.type},<br/>
    X = ${event.clientX}, Y = ${event.clientY},<br/>
    Target Type = ${event.target.type},<br/>
    Target tag Name = ${event.target.tagName}<br/>
  `;
  content.innerHTML += eventDetails;
}

function changeColorOnMouseOver() {
  var control = document.getElementById('btnTest');
  control.style.background = 'red';
  control.style.color = 'yellow';
}

function changeColorOnMouseOut() {
  var control = document.getElementById('btnTest');
  control.style.background = 'black';
  control.style.color = 'yellow';
}

/*
  1. document.getElementById("btnTest").onmouseover = changeColorOnMouseOver

  2. JQuery way

  3. document.getElementById("btnTest").addEventListener(
    "mouseover",
    changeColorOnMouseOver
  )

  To remove the handlers =>
    document.getElementById("btnTest").removeEventListener(
      "mouseover",
      changeColorOnMouseOver
    )

  Note: attachEvent/detachEvent for old versions of browsers.
*/
