export function run() {
  var employee = new Employee("shan", "Raj", "Male", "ksrajbe@gmail.com");

  for (var property in employee) {
    if (typeof employee[property] === "function") {
      continue;
    }
    console.log(`Employee -> name = ${property}, value = ${employee[property]}`);
  }

  PermanentEmployee.prototype = employee;
  var pe = new PermanentEmployee(80000);

  for (var property in pe) {
    if (typeof pe[property] === "function") {
      continue;
    }
    console.log(`PermanentEmployee -> name = ${property}, value = ${pe[property]}`);
  }
}

var Employee = function (firstName, lastName, gender, email) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.gender = gender;
  this.email = email;
}

Employee.prototype.getFullName = function() {
  return `${this.firstName} ${this.lastName}`;
}

Employee.prototype.getGender = function() {
  return `${this.gender}`;
}

Employee.prototype.getEmail = function() {
  return `${this.firstName}'s email is ${this.email}`;
}

var PermanentEmployee = function(annualSalary) {
  this.annualSalary = annualSalary;
}
