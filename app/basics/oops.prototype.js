export function run() {
  console.log(`
    Demo on prototype -> ${new Employee("shan").GetName()}
  `);

  var employee = new Employee("Sh");

  // Overriden at client side
  Employee.prototype.GetName = function () {
    return `'The name is ${this.name.toUpperCase()}'`;
  }

  console.log(`Overriden -> ${employee.GetName()}`);
}

function Employee(name) {
  this.name = name;

  // If we create many objects of employee those many copies
  // will be created and associated to it. Avoid this
  this.getName = function () {
    return this.name;
  }

  Employee.prototype.GetName = function () {
    return this.name;
  }
}
