export function run() {
  whileDemo();
}

function promptDemo() {
  var userInput = Number(prompt("Please enter a number, ", ""));

  if (userInput == 1) {
    console.log("Entered number is => ", userInput);
  } else {
    console.log("Entered number is => ", userInput);
  }

  // Using ternitary
  console.log("Using ternitary => ", userInput == 1 ? "number is 1" : "number is 2");
}

function whileDemo() {
  document.write("While demo !! <br/>");

  var i = 10;

  if (i === 10) {
    console.log("Checking");
  }

  while (true) {
    document.write(i + '<br/>')
    i = i++;
    if (i === 10) {
      break;
    }
  }
}
