export function run() {
  try {
    var circle = new Circle(3);
    console.log(`Circle radius = 3, PI = 3.141 |-> Area = ${circle.CalculateArea()}`);

    // Since PI is static shared by all memory the area value will change here.
    Circle.PI = 10;
    console.log(`Circle radius = 3, PI = 10 |-> Area = ${circle.CalculateArea()}`);
  }
  catch (e) {
    console.log(`Error message -> ${e.message}`);
  }
}

function Circle(radius) {

  this.Radius = radius;

  // Static field
  Circle.PI = 3.141;

  // Static method
  Circle.GetPI = function() {
    return Circle.PI;
  }

  this.CalculateArea = function () {
    return Circle.PI * this.Radius * this.Radius;
  }
}
