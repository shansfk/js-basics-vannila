export function run() {
  console.log(`Recursive function factorial demo => ${factorial(5)}`);
}

function factorial(n) {
  if (n === 0 || n === 1) {
    return 1;
  }
  return n * factorial(n - 1);
}
