export function run() {
  try {
    // var shape = new Shape();

    var Circle = function(shapeName) {
      this.shapeName = shapeName;
    }

    // If we create object this way the constructor function wont be called
    // while we are creating object. All properties will be assigned to
    // the class inherits this.
    Circle.prototype = Object.create(Shape.prototype);

    var circle = new Circle("Circle");
    console.log(`Calling the abstract method (Draw) -> ${circle.Draw()}`);
  }
  catch (e) {
    console.log(`Error details -> ${e.message}`);
  }
}

// TODO : This behaves like a extension metho. So why is required?
function Shape() {
  // TODO : What is the use of this ?
  this.shapeName = "Base shape";
  throw new Error("Cannot create an instance of an abstract class");
}

Shape.prototype.Draw = function() {
  return `Drawing ${this.shapeName}`;
}
