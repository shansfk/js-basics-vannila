export function run() {
  g.preventRightClick(true);

  g.content.append(`
    <h3> Mouse events demo </h3>
    <input type="button" value="Click Me" id="btn"/>
    <input type="button" value="Clear Me" id="clearBtn"/>
    <br/><br/>
    <div id="textArea"/>
  `);

  $('#btn')
    .click(logEvent)
    .mouseup(logEvent)
    .mouseover(logEvent)
    .mouseout(logEvent)
    .dblclick(logEvent)
    .contextmenu(logEvent);

  $('#clearBtn').click(clearText);
}

function logEvent(e) {
  e = event || window.event;
  $("#textArea").append(`${e.type} <br/>`)
}

function clearText() {
  $("#textArea").empty();
}
