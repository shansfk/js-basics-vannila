export let inputText = "";

export function run() {
  g.content.append(`
    <input type="text" id="txtBox" style="width: 250px"/>
    <br/><br/>

    <input type="button" id="btn" value="Process string" style="width: 250px"/>
    <br/><br/>

    <div id="output" style="border:1px solid orange;width:250px;height:250px;"></div>
  `)

  $('#btn').click(processString);
  $('#txtBox').change(function() {
    inputText = $(this).val();
  })
}

function processString(e) {
  if (inputText === null) {
    return;
  }

  var result = inputText.match(/\d+/g);

  var temp = "";

  for (var i = 0; i < result.length; i++) {
    temp += result[i];
  }
  $('#output').append(`
    <br/>
    ${temp}
  `);
}
