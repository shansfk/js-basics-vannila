export function run() {
  var customerInfo = new cust.infoA.customer("shan", "raj");

  // This will retuen -> shan raj undefined
  // when the three parameter is loaded last.
  // console.log(customerInfo.getFullName());

  console.log(customerInfo.getFullName());

  customerInfo = new cust.infoB.customer("SH", "K", "raj");
  console.log(customerInfo.getFullName());
  console.log(customerInfo.firstName);
}

var employeeNew = {
  firstName: "Sfk",
  lastName: "Raj",

  getFullName: function() {
    return this.firstName + ' ' + this.lastName;
  }
}

var cust = cust || {};
cust.infoA = cust.infoA || {};
cust.infoB = cust.infoB || {};

cust.infoA.customer = function(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;

  this.getFullName = function () {
    return `${this.firstName} ${this.lastName}`;
  }

  return this;
}

cust.infoB.customer = function(firstName, middleName, lastName) {
  this.firstName = firstName;
  this.middleName = middleName;
  this.lastName = lastName;

  this.getFullName = function () {
    return `${this.firstName} ${this.middleName} ${this.lastName}`;
  }
}

/*
-- Global scope poullution happens here.

function customer(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;

  this.getFullName = function () {
    return `${this.firstName} ${this.lastName}`;
  }
}

function customer(firstName, middleName, lastName) {
  this.firstName = firstName;
  this.middleName = middleName;
  this.lastName = lastName;

  this.getFullName = function () {
    return `${this.firstName} ${this.middleName} ${this.lastName}`;
  }
}
*/
