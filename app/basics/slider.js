export function run() {

  content.innerHTML = `
    <h3> Slide show </h3>
    <img src="/img/1.jpeg" id=image style="height:150px; width=150px"/>
    <br/>
  `;

  startSlideShow();
  // Runs for some time and stops the startSlideShow
  setTimeout(stopSlideShow, 7000);
}

var id;
var content = document.getElementById('content');

function startSlideShow() {
  content.innerHTML += `<p id="msg">Slide show started !!</p>`;
  id = setInterval(changeImage, 1000);
}

function stopSlideShow() {
  content.innerHTML = `<p>Slide show stopped !!</p>`;
  clearInterval(id);
}

function changeImage() {
  var imageSrc = document.getElementById('image').getAttribute("src");

  var startIndex = imageSrc.lastIndexOf("/") + 1;
  var endIndex = imageSrc.lastIndexOf("/") + 2;
  var currentImageNumber = 1 + Number(imageSrc.substring(startIndex, endIndex));

  if (currentImageNumber > 4) {
    currentImageNumber = 1;
  }

  document.getElementById('image').setAttribute("src", `/img/${currentImageNumber}.jpeg`);
}
