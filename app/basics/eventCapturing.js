var content = $('#content');

export function run() {
  content.append("<h3> Event bubling !! </h3>");

  var div = `
    <div id="wrapId">
      <div id="div1" class="styleClass">
        DIV1
        <div id="div2" class="styleClass">
          DIV2
          <div id="div3" class="styleClass">
          DIV3
          </div>
        </div>
      </div>
    </div>
  `;

  content.append(div);

  var divs = $('#wrapId div');

  for (var i = 0; i < divs.length; i++) {
    // False enables the 'event bubbling', True -> 'event capturing'
    // JQuery not possible here exactly
    divs[i].addEventListener("click", clickHandler, false);
  }
}

function clickHandler(e) {
  // Stops the bubbling/capturing
  e.stopPropagation();
  content.append('<br/>');
  content.append(`${this.getAttribute("id")} click event handled !!.`);
}
