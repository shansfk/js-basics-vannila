export function run() {
  var circle = new Circle();
  console.log(`Calling circle''s (abstract method) draw -> ${circle.draw()}`);
}

var Shape = function() {}
Shape.prototype.draw = function() {
  return "I am a generic shape";
}

var Circle = function() {}
Circle.protype = Object.create(Shape.prototype);
Circle.prototype.draw = function() {
  // TODO : How to call the base draw here?
  return "I am a circle";
}
