var content = $('#content');

export function run() {
  var mainImg = `
    <img height="540px" width="540px"
      style="border:3px solid grey"
      src="/img/1.jpeg" id="mainImage"/>`;

  var allImages = `<br/><div id="myDiv">`;

  for (var i = 1; i < 6; i++) {
    allImages += `
      <img class="imgStyle" id="img_${i}"
        src="/img/${i}.jpeg"/>`;
  }

  allImages += "</div>";

  content.append(`${mainImg} ${allImages}`);

  addOnClick();
  addMouseOverEvents();
}

function addMouseOverEvents() {
  var images = $('#myDiv img');

  for (var i = 0; i < images.length; i++) {

    $(images[i]).mouseover(function() {
      this.style.cursor = 'hand';
      this.style.borderColor = 'red';
    });

    $(images[i]).mouseout(function() {
      this.style.cursor = 'pointer';
      this.style.borderColor = 'grey';
    });
  }
}

function addOnClick() {
  $('#myDiv').click((e) => {
    e = e || window.event;

    var targetElement = e.target || e.srcElement;

    if (targetElement.tagName === "IMG") {
        $("#mainImage").attr("src", targetElement.getAttribute("src"));
    }
  });
}
