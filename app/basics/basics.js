
export function run() {
  console.log("======== Basics Start========= \n");
  basicTypes();
  stringManipulation();
  getEmailDomainParts();
  console.log("======== Basics End======== \n");
}

function basicTypes() {
  var a = "30.89";
  var b = 40;
  console.log("Number conversion (parseInt()) =>", parseInt(a) + 40);
  console.log("Number conversion (parseFloat()) =>", parseFloat(a) + 40);

  // Checking illegal number using isNaN
  console.log("isNaN('shan') =>", isNaN('shan'));

  // Casing
  console.log("upper case =>", "sfk".toUpperCase());

  // Trim
  console.log("Trim =>", "sfk  ".trim());

  // Global replacement (gi - case insensitive)
  var str = "sfk is a greate man. Now sfk is learning the awesome javascript.";
  console.log("Global replacement for word sfk-SH =>", str.replace(/sfk/g, "SH"));
}

function stringManipulation() {
  var str = "javascript (Js) is awesome. Sfk is learning its features and going to start ReactJs";

  // Substring
  console.log("substring (0, 10) => ", str.substring(0,10));
  // substr
  console.log("substr (11) =>", str.substr(11));
  // lastindexof
  console.log("lastindexof (Js) =>", str.lastIndexOf('Js'));
}

function getEmailDomainParts() {
  var email = "shanmugharaj.k@gmail.com";

  var emailPart = email.substring(0, email.indexOf("@"));
  var domainPart = email.substring(email.indexOf("@") + 1);

  console.log("email part =>", emailPart);
  console.log("domain part =>", domainPart);
}
