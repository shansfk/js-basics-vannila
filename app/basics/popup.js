export function run() {
  g.content.append(`
    <h3>Popup window example</h3>
    <input type="button" value="Open popup" id="btn"/>
  `);

  $("#btn").click(onclick);
}

function onclick(e) {
  openInNewWindow('http://google.com');
}

let openInNewTab = (url) => window.open(url);

let openInNewWindow = (url) => window.open(
  url,
  '_blank',
  'height=400,width=400'
);

let openInSameTab = (url) => window.open(
  url,
  '_self',
  'height=400,width=400'
);
