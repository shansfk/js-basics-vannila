export function run() {
  var customer = new Customer("Shanmugha", "raj");

  console.log(`privileged method -> ${customer.getFullName()}`);
  console.log(`public method -> ${customer.GetFullName()}`);
  console.log(`private field -> ${customer._fullName}`);

  // This will throw error
  // console.log(`private method -> ${customer._getFullName()}`);

  // Trying to modify the private field
  customer._fullName = "SFK";

  console.log(`_fullName modified outside (SFK) -> ${customer.GetFullName()}`);

  console.log(`checking the customer._fullName -> ${customer._fullName}`);
}

function Customer(firstName, lastName) {

  // Public fields
  this.firstName = firstName;
  this.lastName = lastName;

  // Private field
  var _fullName;

  // Private function
  function _getFullName() {
    _fullName = `${firstName} ${lastName}`;
    return _fullName;
  }

  // Privileged function
  this.getFullName = function () {
    return _getFullName();
  }

  // Public methods
  Customer.prototype.GetFullName = function() {
    return this.getFullName();
  }
}
