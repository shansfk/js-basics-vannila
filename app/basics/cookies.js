// expires, max-age, domain, path, secure
export function run() {
  window.onload = function () {

    console.log(`Is cookies enabled -> ${docCookies.isCookiesEnabled()}`);

    if (window.document.cookie.length === 0) {
      docCookies.setItem('color', 'lightgrey', 100);
    }

    window.document.bgColor = docCookies.getItem('color');

    docCookies.setItem('sfk', 'He is a genius', null, '/');
    docCookies.setItem('shan', 'Shan is a genius', null, '/');

    console.log(`All cookies -> ${docCookies.keys()}`);

    // To check whether javascript is enabled or not
    g.content.append(`
      <noscript>
        <h3>It seems you have disabled javascript</h3>
      </noscript>
    `);
  }
}

/*
  - Only one cookies can be stored in cookie string
  - Can use JSON.stringify(customObject);
  - Use JSON.parse(strJson) to get object
*/

/*
|*|
|*|  :: cookies.js ::
|*|
|*|  A complete cookies reader/writer framework with full unicode support.
|*|
|*|  https://developer.mozilla.org/en-US/docs/DOM/document.cookie
|*|
|*|  This framework is released under the GNU Public License, version 3 or later.
|*|  http://www.gnu.org/licenses/gpl-3.0-standalone.html
|*|
|*|  Syntaxes:
|*|
|*|  * docCookies.setItem(name, value[, end[, path[, domain[, secure]]]])
|*|  * docCookies.getItem(name)
|*|  * docCookies.removeItem(name[, path], domain)
|*|  * docCookies.hasItem(name)
|*|  * docCookies.keys()
|*|
|*| Notes:
|*|
|*| * end -> max-age here => we can specify the expiry in seconds
|*| * domain -> if we set to 'sfk.com' it is valid for
|*|   sfk.com, sfk.blog.com, sfk.blog.sh.com
*/
var docCookies = {
  isCookiesEnabled: function () {
    var areCookiesEnabled = (navigator.cookieEnabled) ? true : false;

    if (typeof navigator.cookieEnabled === 'undefined' && !areCookiesEnabled) {
      docCookies.setItem('testcookie', '')

      if (docCookies.getItem('testcookie')) {
        areCookiesEnabled = true;
      } else {
        areCookiesEnabled = false;
      }
    }
    return areCookiesEnabled;
  },
  getItem: function (sKey) {
    return decodeURIComponent(window.document.cookie.replace(new RegExp("(?:(?:^|.*;)s*" + encodeURIComponent(sKey).replace(/[-.+*]/g, "$&") + "s*=s*([^;]*).*$)|^.*$"), "$1")) || null;
  },
  setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max-age|path|domain|secure)$/i.test(sKey)) { return false; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
        sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
        break;
        case String:
        sExpires = "; expires=" + vEnd;
        break;
        case Date:
        sExpires = "; expires=" + vEnd.toUTCString();
        break;
      }
    }
    window.document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    return true;
  },
  removeItem: function (sKey, sPath, sDomain) {
    if (!sKey || !this.hasItem(sKey)) { return false; }
    window.document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + ( sDomain ? "; domain=" + sDomain : "") + ( sPath ? "; path=" + sPath : "");
    return true;
  },
  hasItem: function (sKey) {
    return (new RegExp("(?:^|;s*)" + encodeURIComponent(sKey).replace(/[-.+*]/g, "$&") + "s*=")).test(window.document.cookie);
  },
  keys: /* optional method: you can safely remove it! */ function () {
    var aKeys = window.document.cookie.replace(/((?:^|s*;)[^=]+)(?=;|$)|^s*|s*(?:=[^;]*)?(?:1|$)/g, "").split(/s*(?:=[^;]*)?;s*/);
    for (var nIdx = 0; nIdx < aKeys.length; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
      return aKeys;
  }
};
