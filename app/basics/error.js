export function run() {
  tryCatch();
  customThrow();
}

function tryCatch() {
  try {
    console.log(sayHello());
  } catch (e) {
      console.log(`Message = ${e.message}`);
      console.log(`Description = ${e.description}`);
      console.log(`Stack trace = ${e.stack}`);
  } finally {
      console.log("Finished execution");
  }
}

function customThrow() {
  try {
    throw {
      error: "Sfk created this error",
      message: "Error created successfully from sfk !!"
    }
  } catch (e) {
      console.log(`Message = ${e.message}`);
      console.log(`Error = ${e.error}`);
  } finally {
      console.log("Finished execution");
  }
}

// because of babel we cant see this. Still syntax errors cant be catched
// in the try/catch/finally blcoks.
function syntaxErrors() {
  try {
    console.log("Hi");
  } catch(e) {
    console.log(`Message = ${e.message}`);
    console.log(`Description = ${e.description}`);
    console.log(`Stack trace = ${e.stack}`);
  } finally {
      console.log("Finished execution");
  }
}
