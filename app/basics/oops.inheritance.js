export function run() {
  var employee = new Employee("Sfk");
  PermanentEmployee.prototype = employee;

  var pe = new PermanentEmployee(8000);
  console.log(`Calling base method in child (getName) -> ${pe.getName()}`);
  console.log(`Calling base method in child (getNameLength) -> ${pe.getNameLength()}`);
  console.log(`Checking ouwn property (employee.name) -> ${employee.hasOwnProperty('name')}`);
}

function Employee(name) {
  this.name = name;
}

Employee.prototype.getName = function() {
  return this.name;
}

Employee.prototype.getNameLength = function() {
  return this.name.length;
}

function PermanentEmployee(annualSalary) {
  this.annualSalary = annualSalary;
}
