export function run() {
  startClock();
  setTimeout(stopClock, 5000);
}

var id;

function startClock() {
  console.log("Clock started.");
  id = setInterval(setDateTime, 1000);
}

function stopClock() {
  console.log("Clock stopped.");
  clearInterval(id);
}

function setDateTime() {
  document.getElementById('content').innerHTML = new Date();
}
