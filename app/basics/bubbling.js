var content = $('#content');

export function run() {
  content.append("<h3> Event bubling !! </h3>");

  var div = `
    <div id="div" class="styleClass">
      <span id="lbl" class="styleClass">
        <input type="button" id="btn" value="Click me"/>
      </span>
    </div>
  `;

  content.append(div);

  $('#div').click((e) => console.log("DIV Clicked"));
  $('#lbl').click((e) => console.log("SPAN Clicked"));

  // To stop event bubbling.
  $('#btn').click((e) => {
    event = e || window.event;
    if (e.stopPropagation) {
      event.stopPropagation();
    } else {
      event.cancelBubble = true;
    }
    console.log("Button Clicked")
  });
}
