export function run() {
  basic();
  convertingToArrayDemo(29, 30, 28, 5, 15, 99);
}

function basic() {
  // printArgs();
  // printArgs(1, 'shan');
  printArgs(1, 'shan', 'sh');
}

function printArgs() {
  console.log(`Got ${arguments.length} argument's !! \n`);
  for (var i = 0; i < arguments.length; i++) {

    console.log(`Argument ${i} -> ${arguments[i]}`);
  }
}

function convertingToArrayDemo() {
  var arr = Array.prototype.slice.call(arguments);
  // Less than 0: Sort "a" to be a lower index than "b"
  // Zero: "a" and "b" should be considered equal, and no sorting performed.
  // Greater than 0: Sort "b" to be a lower index than "a".
  // http://www.javascriptkit.com/javatutors/arraysort.shtml
  arr.sort(function (a, b) {
      return a - b;
  });
  console.log("Array converted result => ", arr);
}
