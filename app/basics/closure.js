export function run() {
  basicDemo(10, 20);
}

function basicDemo(num1, num2) {
  var result = addNumbers(10, 20);
  console.log("Basic closure => ", "'" + result + "''");
}

function addNumbers(num1, num2) {
  var retVal = "Result (num1 + num2) is = ";

  function add() {
    return retVal + (num1 + num2);
  }
  return add();
}
