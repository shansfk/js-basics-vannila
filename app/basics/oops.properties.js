export function run() {
  try {
    var employee = new Employee("Sh", 30);
    console.log(`Age before change -> ${employee.age}`);

    employee.age = 29;
    console.log(`Age (30->29) -> ${employee.age}`);

    // Will throw error here
    employee.name = "Raj";
    console.log(`Name (Sh->Raj) -> ${employee.age}`);
  }
  catch (e) {
    console.log(`Error -> ${e.message}`);
  }
}

function Employee(name, age) {
  var _age = age;
  var _name = name;

  // Read/Write access
  Object.defineProperty(this, "age", {
    get: function () {
      return _age;
    },
    set: function (value) {
      if (value < 1 || value > 100) {
        console.log("Invalid age");
      } else {
        _age = value;
      }
    }
  });

  // Read-only function
  Object.defineProperty(this, "name", {
    get: function () {
      return _name;
    }
  });
}
