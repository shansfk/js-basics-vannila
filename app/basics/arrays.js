export function run() {
  basics();
  mutators();
  filter();
  removeDuplicates();
  selfInvokingExpression();
}

var array = [2, 10, 20, 50, 13, 99, 1];
var array2 = new Array(3);
var strArray = ["sfk", "sh", "raj", "rathi"];

function basics() {
  console.log("Length of array => ", array.length);

  array2[0] = 100;
  array2[1] = 200;
  array2[2] = 300;
  console.log("Using array ctor => ", array2[0], array2[1], array2[2]);

  array2.push(1);
  array2.push(2);
  console.log("After push => ", array2[3], array2[4]);

  array2.unshift(0);
  console.log("Unshift (adds in the beginning) => ", array2[0]);

  document.write("<h3>Array demos <br/></h3>");
  document.write("The array value => " + array2);
  document.write("<br/> Array length => " + array2.length);
}

// The methods which changes the value of an array is called mutators
function mutators() {
  console.log(strArray.sort());
  console.log(array.sort());

  var sorted = array.sort(
    function (a, b) {
      return a - b;
    }
  );

  console.log(sorted);
  console.log(sorted.reverse());

  var tempArr = [1, 2, 5];
  tempArr.splice(2, 0, 3, 4);
  console.log("splice exmaple ( tempArr.splice(2, 0, 3, 4) only addition) [1, 2, 5] => ", tempArr);

  tempArr = [1, 2, 55, 67, 3];
  tempArr.splice(2, 1, 999);
  console.log("splice exmaple tempArr.splice(2, 1, 999) -> [1, 2, 55, 67, 3] => ", tempArr);
}

function filter() {
  var isEven = function(v, i, a) {
    if (v % 2 === 0) {
      return true;
    } else {
      return false;
    }
  }

  console.log("Filter example => ", array.filter(isEven));
}

function removeDuplicates() {
  strArray.push("sfk")

  var cb = function(v, i, a) {
    return a.indexOf(v) === i;
  };

  console.log("Remove duplicates (sfk) using filter =>", strArray);
  console.log("Remove duplicates (sfk) using filter =>", strArray.filter(cb));
}

function selfInvokingExpression() {
  var factorial = function computeFactorial(num) {
    if (num <= 1) {
      return 1;
    }

    return num * computeFactorial(num - 1);
  }(5);

  console.log("Self Invoking expressions demo (5!) => ", factorial);
}
