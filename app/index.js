import _ from 'lodash';
import * as basics from './basics/basics';
import * as conditionals from './basics/conditionals';
import * as arrays from './basics/arrays';
import * as closure from './basics/closure';
import * as args from './basics/args';
import * as recursive from './basics/recursive';
import * as error from './basics/error';
import * as timingEvents from './basics/timingEvents';
import * as slider from './basics/slider';
import * as events from './basics/events';
import * as bubbling from './basics/bubbling';
import * as thumbnail from './basics/thumbnail';
import * as eventCapturing from './basics/eventCapturing';
import * as browserActions from './basics/browserActions';
import * as mouseEvents from './basics/mouseEvents';
import * as popup from './basics/popup';
import * as regex from './basics/regex';
import * as oops from './basics/oops';
import * as oopsPrivate from './basics/oops.private';
import * as oopsProperties from './basics/oops.properties';
import * as oopsStatic from './basics/oops.static';
import * as oopsPrototype from './basics/oops.prototype';
import * as oopsInheritance from './basics/oops.inheritance';
import * as oopsAbstract from './basics/oops.abstract';
import * as oopsPolymorphism from './basics/oops.polymorphism';
import * as oopsReflection from './basics/oops.reflection';
import * as cookies from './basics/cookies';

cookies.run();
// oopsReflection.run();

// basics.run();
// conditionals.run();
// arrays.run();
// scope.run();
// closure.run();
// args.run();
// recursive.run();
// error.run();
// timingEvents.run();
// slider.run();
// events.run();
// bubbling.run();
// thumbnail.run();
// eventCapturing.run();
// browserActions.run();
// mouseEvents.run();
// popup.run();
// oops.run();
// oopsPrivate.run();
// oopsProperties.run();
// oopsPrototype.run();
// oopsInheritance.run();
// oopsAbstract.run();
// oopsPolymorphism.run();


/*
  To navigate to different page
  window.location = '/path'

  window.location.href
  window.location.hostname
  window.location.protocol
  window.location.pathname
*/
